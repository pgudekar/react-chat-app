import React from 'react';

// This component is where the user can type their message and send it
// to the chat room. We shouldn't communicate with the server here though.
class TypingUser extends React.Component {
	render() {
		return (
			<div className={this.props.typeObj != undefined && this.props.typeObj.textLen > 0 ? 'typing' : 'hide'}>{this.props.typeObj.typist} typing...</div>
	    );
	}
}

TypingUser.defaultProps = {
};

export default TypingUser;
