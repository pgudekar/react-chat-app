require('../styles/ChatApp.css');

import React from 'react';
import io from 'socket.io-client';
import config from '../config';

import Messages from './Messages';
import ChatInput from './ChatInput';

// This is where the main logic of the app will be. Here is where we will
// communicate with the chat server (send and receive messages). We will
// then pass the data received from the server to other components to be
// displayed
class ChatApp extends React.Component {
	socket = {};
	
	constructor(props) {
		super(props);
		this.state = { messages: [], typist: '' };
		this.sendHandler = this.sendHandler.bind(this);
		this.receiveHandler = this.receiveHandler.bind(this);
    
		// Connect to the server
		this.socket = io(config.api, { query: `username=${props.username}` }).connect();

		// Listen for messages from the server
		this.socket.on('server:message', message => {
			this.addMessage(message);
		});
		
		this.socket.on('server:typist', message => {
			this.setState({typeObj: message});
		});
	}

	receiveHandler(typeObj){
		// Emit the message to the server
		this.socket.emit('client:typist', typeObj);
	}
	
	sendHandler(message) {
	    const messageObject = {
	    	username: this.props.username,
	    	message
	    };

	    // Emit the message to the server
	    this.socket.emit('client:message', messageObject);

	    messageObject.fromMe = true;
	    this.addMessage(messageObject);
	}

	addMessage(message) {
		// Append the message to the component state
		const messages = this.state.messages;
		messages.push(message);
		this.setState(message.fromMe ? message : { typeObj: {}, messages });
	}
	  
	render() {
		// Here we want to render the main chat application components
		return (
			<div className="container">
			    <h3>Welcome, {this.props.username} !!!</h3>
			    <Messages messages={this.state.messages} />
			    <ChatInput onReceive={this.receiveHandler} onSend={this.sendHandler} username={this.props.username} typeObj={this.state.typeObj} />
		    </div>
		);
	}
}

ChatApp.defaultProps = {
	username: 'Anonymous'
};

export default ChatApp;
